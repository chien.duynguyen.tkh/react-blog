import axios from 'axios';

export const createBlog = async (formData) => {
  try {
    const response = await axios.post(`http://127.0.0.1/laravel-blog/public/api/blogs`, formData);
    return response.data;
  } catch (error) {
    console.error(error);
    return [];
  }
};