import axios from 'axios';

export const fetchBlogs = async () => {
  try {
    const response = await axios.get("http://127.0.0.1/laravel-blog/public/api/blogs");
    return response.data;
  } catch (error) {
    console.error(error);
    return [];
  }
};
export const searchBlogs = async (term = '') => {
  try {
    const response = await axios.get('http://127.0.0.1/laravel-blog/public/api/blogs/search', { params: { term } });
    return response.data;
  } catch (error) {
    console.error('Error searching blogs:', error);
    return [];
  }
};