import axios from 'axios';

export const getBlogs = async (id) => {
  try {
    const response = await axios.get(`http://127.0.0.1/laravel-blog/public/api/blogs/${id}`);
    return response.data;
  } catch (error) {
    console.error(error);
    return [];
  }
};