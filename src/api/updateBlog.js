import axios from 'axios';

export const updateBlog = async (formData, id) => {
  try {
    const response = await axios.put(`http://127.0.0.1/laravel-blog/public/api/blogs/${id}`, formData);
    return response.data;
  } catch (error) {
    console.error(error);
    return [];
  }
};