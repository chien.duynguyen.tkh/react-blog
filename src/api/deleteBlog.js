import axios from "axios";

export const deleteBlog = async (id) => {
  try {
    const response = await axios.delete(`http://127.0.0.1/laravel-blog/public/api/blogs/${id}`);
    return response.data;
  } catch (error) {
    console.error("Error deleting blog:", error);
    throw error;
  }
};