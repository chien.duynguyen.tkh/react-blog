import '../assets/css/AppSidebar.css'
import {NavLink} from 'react-router-dom'
function CustomNavLink({ to, children, ...rest }) {
  const isActive = to === window.location.pathname;
  const isPending = false;

  return (
    <NavLink
      to={to}
      className={`nav-link ps-3 ${isActive ? "active" : ""} ${isPending ? "pending" : ""}`}
      {...rest}
    >
      {children}
    </NavLink>
  );
}
function AppSidebar() {
  return (
    <div className='sidebar h-100 bg-white'>
      <nav className='navbar p-0'>
        <ul className='navbar-nav w-100'>
          <li className="nav-item">
            <CustomNavLink to={`blog/list`}>
              <i className="bi bi-list-ul me-2"></i>List
            </CustomNavLink>
          </li>
          <li className="nav-item">
            <CustomNavLink to={`blog/new`}>
              <i className="bi bi-plus-square me-2"></i>New
            </CustomNavLink>
          </li>
          <li className="nav-item">
              <CustomNavLink to={`blog/search`}>
                <i className="bi bi-search me-2"></i>Search
              </CustomNavLink>
          </li>
        </ul>
      </nav>
    </div>
  )
}

export default AppSidebar;