import { useState, useEffect } from "react";
import { Link, } from "react-router-dom";
function PaginationBlog(props) {
  const {blogList} = props
  return (
      <nav aria-label="Page navigation example">
        <ul className="pagination m-0 justify-content-center">
            { blogList.current_page > 1 && (
                <li className="page-item">
                <Link className="page-link" href="#" aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </Link>
                </li>
              )
            }
          <li className="page-item"><Link className="page-link" href="#">1</Link></li>
          <li className="page-item"><Link className="page-link" href="#">2</Link></li>
          <li className="page-item"><Link className="page-link" href="#">3</Link></li>
          <li className="page-item">
            <Link className="page-link" href="#" aria-label="Next">
              <span aria-hidden="true">&raquo;</span>
            </Link>
          </li>
        </ul>
      </nav>
  )
}

export default PaginationBlog;