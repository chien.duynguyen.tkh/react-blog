import { useState, useEffect } from "react";
import { useNavigate, useParams } from 'react-router-dom'
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";

function CreateEditBlog(props) {
  const [formData, setFormData] = useState({ title: '', des: '', detail: '', position: [], public: '', category: '', date_public: '' });
  const id = useParams().blogId;
  const [isUpdate, setIsUpdate] = useState(false);
  const { blog } = props;
  const navidate = useNavigate();
  const validationSchema = Yup.object({
    title: Yup.string().required("Title is required"),
    des: Yup.string().required("Description is required"),
    detail: Yup.string().required("Detail is required"),
    position: Yup.array()
      .min(1, "At least one position must be selected")
      .required("Position is required"),
    category: Yup.string().required("Category is required"),
    date_public: Yup.date().required("Date Public is required"),
    thumbnail: Yup.mixed().required("Thumbnail is required").test(
      "fileSize",
      "File size is too large",
      (value) => value && value.size <= 1048576 // 1MB
    ).test(
      "fileType",
      "Invalid file type",
      (value) => value && ['image/jpeg', 'image/png'].includes(value.type)
    )
  });
  useEffect(() => {
    if (!blog) return;
    setFormData(blog);
    setIsUpdate(true);
  }, []);
  const handleChange = e => {
    const { name, value, type, checked } = e.target;
    if (type === 'checkbox') {
      const intValue = parseInt(value, 10);
      const updatedPositions = checked
        ? [...formData.position, intValue] // Add value to array if checked
        : formData.position.filter(pos => pos !== intValue); // Remove value if unchecked
      setFormData({ ...formData, [name]: updatedPositions });
    } else {
      setFormData({ ...formData, [name]: value });
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if (isUpdate) {
      props.onSubmit(formData, id)
    } else {
      props.onSubmit(formData)
    }
    navidate('/blog/list');
  }
  return (
    <div className="card">
      <div className="card-header fs-2">
        {props.action}
      </div>
      <Formik
        initialValues={formData}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting }) => (
          <Form>
            <div className="card-body">
              <div className="px-2">
                <div className="mb-3">
                  <label htmlFor="title" className="form-label">Tiêu đề:</label>
                  <Field type="text" value={formData.title ?? ""} name="title" className="form-control" id="title" onChange={handleChange} />
                  <ErrorMessage
                    name="title"
                    component="div"
                    className="text-danger"
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="description" className="form-label">Mô tả ngắn:</label>
                  <Field as="textarea" className="form-control" name="des" value={formData.des ?? ""} id="description" rows="3" onChange={handleChange} />
                  <ErrorMessage
                    name="des"
                    component="div"
                    className="text-danger"
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="detail" className="form-label">Chi tiết:</label>
                  <Field as="textarea" className="form-control" name="detail" value={formData.detail ?? ""} id="detail" rows="5" onChange={handleChange} />
                  <ErrorMessage
                    name="detail"
                    component="div"
                    className="text-danger"
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="thumbnail" className="form-label">Hình ảnh:</label>
                  <Field type="file" name="thumbnail" className="form-control" id="thumbnail" />
                  <ErrorMessage
                    name="thumbnail"
                    component="div"
                    className="text-danger"
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="position" className="form-label">Vị trí:</label><br></br>
                  {Object.entries(props.positions).map(([index, position]) => (
                    <div className="form-check form-check-inline" key={index}>
                      <Field
                        className="form-check-input"
                        type="checkbox"
                        name="position"
                        id={`position${index}`}
                        value={parseInt(index, 10)}
                        checked={formData.position ? formData.position.includes(parseInt(index, 10)) : false}
                        onChange={handleChange}
                      />
                      <label className="form-check-label" htmlFor={`position${index}`}>
                        {position}
                      </label>
                    </div>
                  ))}
                  <ErrorMessage
                    name="position"
                    component="div"
                    className="text-danger"
                  />
                </div>
                <div className="mb-3">
                  <label htmlFor="public" className="form-labe">Public:</label><br></br>
                  <div className="form-check form-check-inline">
                    <Field
                      className="form-check-input"
                      type="radio"
                      name="public"
                      id="public-1"
                      value="1"
                      checked={formData.public == 1}
                      onChange={handleChange}
                    />
                    <label className="form-check-label" htmlFor="public-1">
                      Yes
                    </label>
                  </div>
                  <div className="form-check form-check-inline">
                    <Field
                      className="form-check-input"
                      type="radio"
                      name="public"
                      id="public-0"
                      value="0"
                      checked={formData.public == 0}
                      onChange={handleChange}
                    />
                    <label className="form-check-label" htmlFor="public-0">
                      No
                    </label>
                  </div>
                </div>
                <div className="row mb-3">
                  <div className="col">
                    <label htmlFor="category" className="form-label">Loại:</label>
                    <select
                      className="form-select"
                      name="category"
                      aria-label="Default select example"
                      value={formData.category ?? ""} // Bind select value to state
                      onChange={handleChange}
                    >
                      <option value="" disabled>Choose...</option>
                      {Object.entries(props.category).map(([index, category]) => (
                        <option value={index} key={index}>{category}</option>
                      ))}
                    </select>
                    <ErrorMessage
                      name="date_public"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                  <div className="col">
                    <label htmlFor="date_public" className="form-label">Date Public:</label>
                    <Field type="date" name="date_public" value={formData.date_public ?? ""} className="form-control" id="date_public" onChange={handleChange} />
                    <ErrorMessage
                      name="date_public"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="card-footer text-muted text-center">
              <button type="submit" className="btn btn-success me-1" disabled={isSubmitting}>Submit</button>
              <button type="button" className="btn btn-primary">Clear</button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  )
}

export default CreateEditBlog;