import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import PaginationBlog from '../Pagination';
function ListBlog(props) {
  const [searchInput, setSearchInput] = useState('');
  const navigate = useNavigate();
  const location = useLocation();
  const { blogList } = props;
  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const searchTerm = params.get('term');
    if (searchTerm) {
      setSearchInput(searchTerm);
    }
  }, [location.search]);
  const handleSubmit = async (e) => {
    e.preventDefault();
    props.onSearchSubmit(searchInput);
    if (searchInput) {
      navigate(`/blog/search?term=${searchInput}`);
    } else {
      navigate(`/blog/search`);
    }
  };
  const handleDelete = (id) => {
    props.onDelete(id)
  };
  const columns = props.columns
  const listColumn = columns.map((column) =>
    <th scope="col" key={column}>{column}</th>
  );
  const positions = [
    { id: 1, name: "Việt nam" },
    { id: 2, name: "Châu á" },
    { id: 3, name: "Châu âu" },
    { id: 4, name: "Châu mỹ" },
  ]

  const category = {
    0: "Kinh doanh",
    1: "Tài chính",
    2: "Xây dựng"
  }
  return (
    <>
      {props.formSearch ?
        <div className="card mb-2">
          <div className="card-header fs-2">
            Search Blogs
          </div>
          <form onSubmit={handleSubmit}>
            <div className="card-body">
              <div className="mb-3 row">
                <label htmlFor="search" className="form-label col-sm-2">Title:</label>
                <div className="col-sm-10">
                  <input
                    type="text"
                    className="form-control"
                    id="searchTerm"
                    name="term"
                    value={searchInput}
                    onChange={(e) => setSearchInput(e.target.value)}
                    placeholder="Search"
                  />
                </div>
              </div>
              <div className='text-center'>
                <button type="submit" className="btn btn-outline-success">Search</button>
              </div>
            </div>
          </form>
        </div>
        : null
      }
      <div className="card">
        <h5 className="card-header">List Blog</h5>
        <div className="card-body text-center p-0">
          <table className="table table-bordered">
            <thead>
              <tr>
                {listColumn}
              </tr>
            </thead>
            <tbody>
              {blogList && blogList.data ? (
                blogList.data.map((blog) => (
                  <tr key={blog.id}>
                    <td>{blog.id}</td>
                    <td>{blog.title}</td>
                    <td>{category[blog.category]}</td>
                    <td>{blog.public ? "Yes" : "No"}</td>
                    <td>{
                      positions.reduce((string, item) => {
                        if (blog.position.includes(item.id) && blog.position !== null) {
                          return string + item.name + ", "
                        } else {
                          return string + ""
                        }
                      }, "").slice(0, -2)
                    }
                    </td>
                    <td>{blog.date_public}</td>
                    <td><Link to={`/blog/edit/${blog.id}`} role="button" className="btn btn-outline-primary" aria-disabled="true">Edit</Link></td>
                    <td><button type="button" className="btn btn-outline-danger" onClick={() => handleDelete(blog.id)} aria-disabled="true">Delete</button></td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td colSpan="7">No blogs found</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <div className="card-footer text-center">
          <PaginationBlog blogList={blogList}/>
        </div>
      </div>
    </>
  )
}

export default ListBlog;