import logo from'../logo.svg'
import React from 'react';
function AppHeader() {
  const styles = {
    header: {
      color: "white",
      height: "56px"
    }
  }
  return (
    <header className="App-header fixed-top bg-black row" style={styles.header}>
      <div className='col-2 d-flex justify-content-center align-items-center'>
        <img src={logo} alt="Logo" width="50"/>
      </div>
      <div className='col-10 d-flex justify-content-center align-items-center"'>
        <h1 className='m-0'>BlogManangement</h1>
      </div>
    </header>
  )
}

export default AppHeader;