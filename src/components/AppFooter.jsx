function AppFooter() {
  const styles = {
    footer: {
      height: "50px",
      color: "white",
    }
  }
  return (
    <footer className='footer fixed-bottom bg-secondary d-flex justify-content-center align-items-center' style={styles.footer}>
      <span>Copyright <i className="bi bi-c-circle"></i> 2021 <a href='#'>Devfast</a></span>
    </footer>
  )
}
  
export default AppFooter;