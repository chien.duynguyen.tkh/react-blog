import { createBlog } from "../../api/createBlog";
import CreateEditBlog from "../../components/blog/CreatUpdateBlog";
import React from 'react';
function BlogCreate() {
  const action = "New Blogs";
  const positions = {1: "Việt Nam", 2: "Châu Á", 3: "Châu Âu", 4: "Châu Mỹ"}
  const category = {
    0: "Kinh doanh",
    1: "Tài chính",
    2: "Xây dựng"
  }
  const handleCreateSubmit = async (formData) => {
    try {
      const data = await createBlog(formData);
      alert(data.message)
    } catch (error) {
      console.error('Error create blogs:', error);
    }
  };
  return (
    <CreateEditBlog onSubmit={handleCreateSubmit} action={action} positions={positions} category={category}/>
  )
}
  
export default BlogCreate;