import ListBlog from "../../components/blog/ListBlog";
import { fetchBlogs, searchBlogs } from "../../api/listBlog";
import { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
function BlogSearch() {
 
  const columns = ['id', 'Tin', 'Loại', 'Trạng thái', 'Vị trí', 'Ngày public', 'Edit', 'Delete']
  const formSearch = true
  const [blogs, setBlogs] = useState([]);
  const navigate = useNavigate();

  const location = useLocation();
  useEffect(() => {
    const searchParams = new URLSearchParams(location.search);
    const searchTerm = searchParams.get("term");
    if(searchTerm) {
      navigate(`/blog/search?term=${searchTerm}`);
    } else {
      navigate(`/blog/search`);
    }
    if (searchTerm) {
      const searchBlogsAndSet = async () => {
        const blogsData = await searchBlogs(searchTerm);
        setBlogs(blogsData);
      };
      searchBlogsAndSet();
    } else {
      const getBlogs = async () => {
        const blogsData = await fetchBlogs();
        setBlogs(blogsData);
      };
      getBlogs()
    }
  }, [location.search]);
  const handleDelete = async (id) => {
    try {
      const response = await deleteBlog(id);
      alert(response.message)
      setBlogs(blogs.filter(blog => blog.id !== id));
    } catch (error) {
      console.error('Error deleting blog:', error);
    }
  };
  const handleSearchSubmit = async (searchTerm) => {
    try {
      const filteredBlogs = await searchBlogs(searchTerm);
      // console.log(filteredBlogs);
      setBlogs(filteredBlogs)
    } catch (error) {
      console.error('Error searching blogs:', error);
    }
  };
  return (
    <>
      <ListBlog 
        columns={columns} 
        formSearch={formSearch} 
        blogList={blogs} 
        onSearchSubmit={handleSearchSubmit }
        onDelete={handleDelete}
      />
    </>
  )
}
  
export default BlogSearch;