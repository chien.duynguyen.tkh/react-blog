import ListBlog from "../../components/blog/ListBlog";
import {useState, useEffect} from 'react';
import { fetchBlogs } from "../../api/listBlog";
import { deleteBlog } from "../../api/deleteBlog";
function BlogList() {
  const columns = ['id', 'Tin', 'Loại', 'Trạng thái', 'Vị trí', 'Ngày public', 'Edit', 'Delete']
  const formSearch = false
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    const getBlogs = async () => {
      const blogsData = await fetchBlogs();
      setBlogs(blogsData);
    };
    getBlogs();
  }, []);
  const handleDelete = async (id) => {
    try {
      const response = await deleteBlog(id);
      alert(response.message)
      setBlogs(blogs.filter(blog => blog.id !== id));
    } catch (error) {
      console.error('Error deleting blog:', error);
    }
  };

  return (
    <>
      <ListBlog columns={columns} formSearch={formSearch} blogList={blogs} onDelete={handleDelete}/>
    </>
  )
}
  
export default BlogList;