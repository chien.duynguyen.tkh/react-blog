import CreateEditBlog from "../../components/blog/CreatUpdateBlog";
import { updateBlog } from "../../api/updateBlog";
import { useParams} from 'react-router-dom'
import { getBlogs } from "../../api/getBlog";
import { useEffect, useState } from "react";
function BlogEdit() {
  const action = "Edit Blog"
  const [blog, setBlog] = useState({});
  const positions = {1: "Việt Nam", 2: "Châu Á", 3: "Châu Âu", 4: "Châu Mỹ"}
  const { blogId } = useParams();
  const category = {
    0: "Kinh doanh",
    1: "Tài chính",
    2: "Xây dựng"
  }
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    async function fetchBlog() {
      try {
        const blogsData = await getBlogs(blogId);
        if (typeof blogsData.position === 'string') {
          blogsData.position = JSON.parse(blogsData.position).map(pos => parseInt(pos, 10));
        }
        setBlog(blogsData);
        setLoading(false);
        
      } catch (error) {
        console.error('Error fetching blog:', error);
      }
    };
    fetchBlog();
  }, []);
  const handleUpdateSubmit = async (formData, id) => {
    try {
      const data = await updateBlog(formData, id);
      alert(data.message)
    } catch (error) {
      console.error('Error create blogs:', error);
    }
  };
  if (loading) {
    return <div>Loading...</div>;
  }
  return (
    <CreateEditBlog blog={blog} onSubmit={handleUpdateSubmit} action={action} positions={positions} category={category}/>
  )
}
  
export default BlogEdit;