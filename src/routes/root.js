import AppHeader from '../components/AppHeader';
import AppSidebar from '../components/AppSidebar';
import AppFooter from '../components/AppFooter';
import { Outlet } from "react-router-dom";
import React from 'react';
export default function Root() {
  const styles = {
    main: {
      display: 'flex',
      flexDirection: 'column',
      height: '100vh'
    },
    contentWrapper: {
      flexDirection: 'column',
      flexGrow: 1,
      overflow: 'hidden',
    },
    content: {
      flexGrow: 1,
      overflow: 'hidden',
      paddingLeft: '250px', // adjust as per your sidebar width
    },
    mainContent: {
      paddingTop: '80px',   // adjust as per your header height
      paddingBottom: '50px', // add paddingBottom to account for the footer height
      height: '100%',
      overflowY: 'auto',
      width: '100%',
    },
  };
  return (
    <>
      <div className="App bg-light" style={styles.main}>
        <div className='container-fluid'>
          <AppHeader />
        </div>
        <div className="d-flex mb-3" style={styles.contentWrapper}>
          <div className="d-flex" style={styles.content}>
            <AppSidebar />
            <div className="px-3" style={styles.mainContent}>
              <Outlet />
            </div>
          </div>
        </div>      
        <AppFooter />
      </div>
    </>
  );
}