import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import 'bootstrap-icons/font/bootstrap-icons.min.css'

import {createBrowserRouter,RouterProvider,} from "react-router-dom";
import ErrorPage from './pages/error';
import BlogList from './pages/blog/list';
import BlogCreate from './pages/blog/create';
import BlogSearch from './pages/blog/search';
import BlogEdit from './pages/blog/update';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "blog/list",
        element: <BlogList />,
      },
      {
        path: "blog/new",
        element: <BlogCreate />,
      },
      {
        path: "blog/edit/:blogId",
        element: <BlogEdit />,
      },
      {
        path: "blog/search",
        element: <BlogSearch />,
      }
    ],
  },
]);
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
